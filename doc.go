/* Package async contains code to assist with using Go's asyncronous
* capabilities by implementing common best practices and well-used design
* patterns.
 */
package async
