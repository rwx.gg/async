package async_test

import (
	"fmt"

	"gitlab.com/rwx.gg/async"
)

func ExampleForEach_sync() {

	names := []interface{}{"Rob", "Doris", "Nick", "Maxx",
		"Ben", "Otto", "Gordon", "Ethan"}

	act := func(n interface{}) error {
		fmt.Printf("%v\n", n)
		return nil
	}

	async.ForEach(names, act)

	// Output:
	// Rob
	// Doris
	// Nick
	// Maxx
	// Ben
	// Otto
	// Gordon
	// Ethan
}

func ExampleForEach_syncerr() {

	names := []interface{}{"Rob", "Doris", "Nick", "Maxx",
		"Ben", "Otto", "Gordon", "Ethan"}

	act := func(n interface{}) error {
		fmt.Printf("%v\n", n)
		return nil
	}

	err := make(chan error, 8)
	async.ForEach(names, act, 0, err)
	for e := range err {
		fmt.Print(e)
	}

	// Output:
	// Rob
	// Doris
	// Nick
	// Maxx
	// Ben
	// Otto
	// Gordon
	// Ethan
	// <nil><nil><nil><nil><nil><nil><nil><nil>
}

func ExampleForEach_asyncnoerr() {

	names := []interface{}{"Rob", "Doris", "Nick", "Maxx",
		"Ben", "Otto", "Gordon", "Ethan"}

	act := func(n interface{}) error {
		fmt.Printf("%v\n", n)
		return nil
	}

	async.ForEach(names, act, 8)

	// Unordered Output:
	// Rob
	// Doris
	// Nick
	// Maxx
	// Ben
	// Otto
	// Gordon
	// Ethan
}

func ExampleForEach_asyncerr() {

	names := []interface{}{"Rob", "Doris", "Nick", "Maxx",
		"Ben", "Otto", "Gordon", "Ethan"}

	act := func(n interface{}) error {
		if n.(string) == "Rob" {
			return fmt.Errorf("Rob is not allowed.")
		}
		fmt.Printf("%v\n", n)
		return nil
	}

	err := make(chan error, 8)
	async.ForEach(names, act, 8, err)
	for e := range err {
		if e != nil {
			fmt.Print(e)
		}
	}

	// Unordered Output:
	// Doris
	// Nick
	// Maxx
	// Ben
	// Otto
	// Gordon
	// Ethan
	// Rob is not allowed.
}
