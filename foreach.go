package async

// Action is a function that will be passed an item to act upon and must
// return an error (which can be nil).
type Action func(item interface{}) error

// ForEach runs the given action on each item in items. By default each
// item is handled sequentially. If the second argument is greater than
// 1 then a number of goroutines up to that limit will be created and
// the action on each will be asynchronous and in no particular
// sequence.  The third parameter is a channel that will be sent the
// return values of each action error return value as completes. After
// all have completed the channel will be closed. Passing a buffered
// channel with less than Count (number of items) will block until the
// return values are read. Block on return value reads does not block
// the processing of actions on items in any way. If the channel passed
// is nil error results will be ignored but the actions will still run.
// If the action is nil a panic will occur.
//
// Note that the concurrent goroutine limit can be rather high.
// Goroutines are very light weight and, according to the docs, "it is
// practical to create hundreds of thousands of goroutines in the same
// address space." This depends on what the action is, however. Forking
// a subprocess command could require a lower limit but, for most modern
// devices, is still probably in the thousands. On the other hand, if
// you know you don't need a high degree of concurrency you can keep the
// limit rather small and reduce the slight overhead of setting up the
// internal concurrency channel semaphore.  Benchmark testing can help
// determine the best limit for a particular action more objectively.
//
func ForEach(a ...interface{}) {
	var items []interface{}
	var lim int
	var rvals chan error
	var act Action
	var ok bool

	switch len(a) {
	case 2:
		items, ok = a[0].([]interface{})
		if !ok {
			return
		}
		act, ok = a[1].(func(i interface{}) error)
		if !ok {
			return
		}
	case 3:
		items, ok = a[0].([]interface{})
		if !ok {
			return
		}
		act, ok = a[1].(func(i interface{}) error)
		if !ok {
			return
		}
		lim, ok = a[2].(int)
		if !ok {
			return
		}
	case 4:
		items, ok = a[0].([]interface{})
		if !ok {
			return
		}
		act, ok = a[1].(func(i interface{}) error)
		if !ok {
			return
		}
		lim, ok = a[2].(int)
		if !ok {
			return
		}
		rvals, ok = a[3].(chan error)
		if !ok {
			return
		}
	default:
		return
	}

	// one at a time
	if lim == 0 {
		if rvals == nil {
			for _, item := range items {
				act(item)
			}
			return
		}
		for _, item := range items {
			rvals <- act(item)
		}
		close(rvals)
		return
	}

	// limited channel semaphore idiom (from net pkg)
	sem := make(chan interface{}, lim)
	for _, item := range items {
		sem <- true // blocks if full
		if rvals == nil {
			go func(item interface{}) {
				defer func() { <-sem }() // frees
				act(item)
			}(item)
			continue
		}
		go func(item interface{}) {
			defer func() { <-sem }() // frees
			rvals <- act(item)
		}(item)
	}
	// waits for all (keeps filling until full again)
	for i := 0; i < cap(sem); i++ {
		sem <- true
	}
	// all goroutines have now finished
	if rvals != nil {
		close(rvals)
	}
}
